# README - Display IP address on SenseHat #
This project works for SenseHat mounted on top of Raspberry Pi 3. It will display the IP address
of wlan0 on the SenseHat.


> **Note:** This project has been coded using python3.5. But it should be able to run in python2.7.



## Installation Steps ##
* sudo apt-get update
* sudo apt-get install sense-hat


## To show the IP address ##
./start_light.py &


## To stop showing the IP address ##
./stop_light.py
- This is done by killing the pid of start_light.py