#!/usr/bin/python
from sense_hat import SenseHat


import socket
import fcntl
import struct

def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0X8915, struct.pack('256s', ifname[:15]))[20:24])

sense = SenseHat()
try:
    while True:    
        ipaddress = get_ip_address("wlan0")
        sense.show_message(ipaddress)
except Exception:
    print("No ip address")
    pass

  
